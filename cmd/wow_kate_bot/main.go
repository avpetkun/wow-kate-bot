package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"image"
	"image/draw"
	"image/jpeg"
	"image/png"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"

	tbot "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/nfnt/resize"
)

var botToken = "1935079088:AAGOUPDMR_xr6_FML1974p0flmapcEB3tus"

func main() {
	log.Println("start")

	os.MkdirAll("results", os.ModePerm)

	tmplImage, err := fetchTemplateImage()
	if err != nil {
		log.Fatal(err)
	}

	bot, err := tbot.NewBotAPI(botToken)
	check(err)

	tbotUpdateConf := tbot.NewUpdate(0)
	tbotUpdates := bot.GetUpdatesChan(tbotUpdateConf)

	log.Println("started")

	for update := range tbotUpdates {
		if update.Message == nil {
			continue
		}
		go handleMessage(bot, tmplImage, update.Message)
	}
}

func savePhoto(msgID int, username string, data []byte) {
	path := "results/" + strconv.Itoa(msgID) + "_" + username + ".jpg"
	ioutil.WriteFile(path, data, os.ModePerm)
}

func getUsername(chat *tbot.Chat) string {
	if chat.UserName == "" {
		return chat.FirstName + " " + chat.LastName
	}
	return chat.FirstName + " " + chat.LastName + " (" + chat.UserName + ")"
}

func handleMessage(bot *tbot.BotAPI, tmplImage image.Image, msg *tbot.Message) {
	if handleUserImage(bot, tmplImage, msg) {
		log.Printf("processed user %s\n", getUsername(msg.Chat))
	} else {
		response(bot, msg, "Отправьте фото. В описании можете указать процент покрытия")
	}
}

func fetchTemplateImage() (image.Image, error) {
	data, err := ioutil.ReadFile("template.png")
	if err != nil {
		return nil, err
	}
	return png.Decode(bytes.NewReader(data))
}

func response(bot *tbot.BotAPI, to *tbot.Message, text string) {
	msg := tbot.NewMessage(to.Chat.ID, text)
	_, err := bot.Send(msg)
	if err != nil {
		log.Println(err)
	}
}

func handleUserImage(bot *tbot.BotAPI, tmplImage image.Image, msg *tbot.Message) bool {
	percent, _ := strconv.Atoi(strings.Trim(strings.TrimSpace(msg.Caption), "%"))
	if len(msg.Photo) < 1 {
		return false
	}

	response(bot, msg, "Обработка...")

	userPhoto := msg.Photo[len(msg.Photo)-1]
	fileLink, err := fetchFileLink(userPhoto.FileID)
	if err != nil {
		log.Println(err)
		return false
	}

	fileData, err := downloadFile(fileLink)
	if err != nil {
		log.Println(err)
		return false
	}

	userImg, err := jpeg.Decode(bytes.NewReader(fileData))
	if err != nil {
		log.Println(err)
		return false
	}

	mergedImg, err := mergeImages(tmplImage, userImg, percent)
	if err != nil {
		log.Println(err)
		return false
	}

	var dst bytes.Buffer
	if err = jpeg.Encode(&dst, mergedImg, nil); err != nil {
		log.Println(err)
		return false
	}
	photoData := dst.Bytes()

	photoFile := tbot.NewPhotoUpload(msg.Chat.ID, tbot.FileBytes{
		Name:  "photo.jpg",
		Bytes: photoData,
	})

	sent, err := bot.Send(photoFile)
	if err != nil {
		log.Println(err)
		return false
	}

	savePhoto(sent.MessageID, getUsername(msg.Chat), photoData)
	return true
}

func mergeImages(tmplImg, userImg image.Image, percent int) (image.Image, error) {
	if percent < 1 {
		percent = 50
	}
	if percent > 100 {
		percent = 100
	}

	rgba := image.NewRGBA(userImg.Bounds())

	uw := userImg.Bounds().Dx()
	uh := userImg.Bounds().Dy()

	maxW := uint(float64(uw) * (float64(percent) / 100))
	maxH := uint(float64(uh) * (float64(percent) / 100))

	resized := resize.Thumbnail(maxW, maxH, tmplImg, resize.Bicubic)
	rw := resized.Bounds().Dx()
	rh := resized.Bounds().Dy()
	rect := image.Rect(uw-rw, uh-rh, uw, uh)

	draw.Draw(rgba, userImg.Bounds(), userImg, image.Point{}, draw.Src)
	draw.Draw(rgba, rect, resized, image.Point{}, draw.Over)

	return rgba, nil
}

func fetchFileLink(fileID string) (link string, err error) {
	url := fmt.Sprintf("https://api.telegram.org/bot%s/getFile?file_id=%s", botToken, fileID)
	res, err := http.Get(url)
	if err != nil {
		return "", err
	}
	defer res.Body.Close()
	var result struct {
		OK     bool `json:"ok"`
		Result struct {
			FileID       string `json:"file_id"`
			FileUniqueID string `json:"file_unique_id"`
			FileSize     int    `json:"file_size"`
			FilePath     string `json:"file_path"`
		} `json:"result"`
	}
	err = json.NewDecoder(res.Body).Decode(&result)
	if err != nil {
		return "", err
	}

	link = fmt.Sprintf("https://api.telegram.org/file/bot%s/%s", botToken, result.Result.FilePath)
	return
}

func downloadFile(link string) ([]byte, error) {
	res, err := http.Get(link)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	return ioutil.ReadAll(res.Body)
}

func check(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
