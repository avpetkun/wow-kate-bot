module project

go 1.16

require (
	github.com/go-telegram-bot-api/telegram-bot-api/v5 v5.0.0-rc1
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
)
